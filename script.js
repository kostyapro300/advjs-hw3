// HW ADVANCHED JS
// Деструктуризація - це спосіб отримати доступ до окремих 
// елементів складних даних, таких як об'єкти або масиви, за 
// допомогою простого синтаксису. Замість того, щоб кожен раз 
// звертатися до кожного елементу окремо, ми можемо одним рядком коду 
// витягти всі значення, які нас цікавлять, і присвоїти їх змінним.

// ЗАВДАННЯ 1 

const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];

const [client1, client2, client3, client4, client5, client6, client7] = clients1;
const [client8, client9, client10, client11] = clients2;

const unionClients = [client1, client2, client3, client4, client5, client6, client7, client8, client9, client10, client11];

console.log(unionClients);

// Завдання 2 

const characters = [
    {
      name: "Елена",
      lastName: "Гилберт",
      age: 17, 
      gender: "woman",
      status: "human"
    },
    {
      name: "Кэролайн",
      lastName: "Форбс",
      age: 17,
      gender: "woman",
      status: "human"
    },
    {
      name: "Аларик",
      lastName: "Зальцман",
      age: 31,
      gender: "man",
      status: "human"
    },
    {
      name: "Дэймон",
      lastName: "Сальваторе",
      age: 156,
      gender: "man",
      status: "vampire"
    },
    {
      name: "Ребекка",
      lastName: "Майклсон",
      age: 1089,
      gender: "woman",
      status: "vempire"
    },
    {
      name: "Клаус",
      lastName: "Майклсон",
      age: 1093,
      gender: "man",
      status: "vampire"
    }
  ];

const charactersShortInfo = characters.map(({ name, lastName, age }) => ({ name, lastName, age }));


console.log(charactersShortInfo);

// Завдання 3 

const user1 = {
    name: "John",
    years: 30
};
  
const {name: userName, years: userAge, isAdmin = false } = user1 

console.log(userName);
console.log(userAge);
console.log(isAdmin);

// Завдання 4 
const satoshi2020 = {
    name: 'Nick',
    surname: 'Sabo',
    age: 51,
    country: 'Japan',
    birth: '1979-08-21',
    location: {
      lat: 38.869422, 
      lng: 139.876632
    }
  }
  
  const satoshi2019 = {
    name: 'Dorian',
    surname: 'Nakamoto',
    age: 44,
    hidden: true,
    country: 'USA',
    wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
    browser: 'Chrome'
  }
  
  const satoshi2018 = {
    name: 'Satoshi',
    surname: 'Nakamoto', 
    technology: 'Bitcoin',
    country: 'Japan',
    browser: 'Tor',
    birth: '1975-04-05'
  }

const fullprofile = {
    ...satoshi2018,
    ...satoshi2019,
    ...satoshi2020
}
console.log(fullprofile);

// Завдання 5 

const books = [{
  name: 'Harry Potter',
  author: 'J.K. Rowling'
}, {
  name: 'Lord of the Rings',
  author: 'J.R.R. Tolkien'
}, {
  name: 'The Witcher',
  author: 'Andrzej Sapkowski'
}];

const bookToAdd = {
  name: 'Game of Thrones',
  author: 'George R.R. Martin'
};

const { name, author } = bookToAdd; 

const allBooks = [...books, { name, author }];

console.log(allBooks); 




// завдання 6 
const employee = {
    name: 'Vitalii',
    surname: 'Klichko'
}

const newEmploye = {
    ...employee, 
    age: 30, 
    salary: 15000
}

console.log(newEmploye);

// завдання 7 

const array = ['value', () => 'showValue'];

// Допишіть код тут

const [value, showValue] = array

alert(value); // має бути виведено 'value'
alert(showValue());  // має бути виведено 'showValue'